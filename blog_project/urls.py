"""blog_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from blog_app import views
urlpatterns = [
    
    path('admin/', admin.site.urls),
    url(r'^sign_up/',views.user_registeration)
    url(r'^upload_blog/',views.upload_blog)
    url(r'^fetching_blog/',views.fetching_blog)
    url(r'^like_comment_blog/',views.like_comment_blog)
    url(r'^all_user_uploded_blog_info/',views.all_user_uploded_blog_info)
    url(r'^dashboard/',views.dashboard)
    url(r'^purticular_blog/',views.purticular_blog)


    
]
