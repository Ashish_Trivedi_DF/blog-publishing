from django.shortcuts import render
import json
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from pymongo import MongoClient
from cache_memoize import cache_memoize
import models
# Create your views here.

#API FOR CREATING USER 

@csrf_exempt 
def user_registeration(request):

    db =models.db_connection
    mobile=request.POST.get("mobile",None)
    if "mobile" not in request.POST:
        response ={"Status" : False , "Message":"Please Enter Mobile Number  !!!","Status Code": 400}

        return JsonResponse(response)

    db_collection = db.user_info

    user_exist = db_collection.find_one({"mobile":mobile})
    # type(user_exist),user_exist
    if user_exist is None:
        print("not exist")
        post_data = {'mobile':mobile}
        result = posts.insert_one(post_data)
        response ={"Status" : True , "Message":"You are registered now  !!!","statusCode": 200}

    else:
        response ={"Status" : "Success" , "Message":"User Already Exist !!!","statusCode": 200}
        
    return JsonResponse(response)



#API FOR UPLOAD BLOG  

@csrf_exempt 
def upload_blog(request):

    if "mobile" not in request.POST:
        response ={"Status" : False , "Message":"Please Enter Mobile Number  !!!","Status Code": 400}

        return JsonResponse(response)

    if "contain" not in request.POST:
        response ={"Status" : False , "Message":"Please Write Some Conatain  !!!","Status Code": 400}

        return JsonResponse(response)

    db =models.db_connection 
    mobile =request.POST.get('mobile',None)
    containt =request.POST.get('contain',None)
    user_exist = db.user_info.find_one({"mobile":mobile})
    db_collection = db.user_blog

    if user_exist != None:
        post_data = {containt: [{'comment':[]},{'likes':[]},{'dislikes':[]}],'mobile':mobile,'views':0}
        result = db_collection.insert(post_data)
        response ={"Status" : True , "Message":"Blog Uploaded Successfully   !!!","statusCode": 200}

    else:
        response ={"Status" : "Success" , "Message":"User Not Registerd, You Can not upload Blog!!!","statusCode": 200}

    return JsonResponse(response)


    





#API FOR FETCHING BLOG  

@csrf_exempt 
def fetching_blog(request):

    db = models.db_connection 
    db_collection = db.user_blog
    Blog_Contain = db_collection.find()

    Blog_Inforamation_For_Reader =[]
    user_feed={"feed":None}

    for data in Blog_Contain:

        containt = str(list(data.keys())[1])
        Blog_Inforamation_For_Reader.append({containt:data['mobile'],'comment':data[containt][0]['comment'],'likes':data[containt][1]['likes'],'dislikes':data[containt][2]['dislikes'],'Views':data['views']})

    user_feed={"feed":Blog_Inforamation_For_Reader }
    response ={"Status" : "Success" , "Blog_Data":user_feed,"statusCode": 200}
        
    return JsonResponse(response)

        


#API FOR LIKE ,COMMENT ,DISLIKES


@csrf_exempt 
def like_comment_blog(request):

    db = models.db_connection 
    db_collection = db.user_blog

    like_comment_blog_info = request.POST.get("blog_data",None)

    # {'Facebook is social media platform it gives you informative and relavent post':[{'mobile': 9752284010},{'comment': "Not Worthy"}, {'likes': False}, {'dislikes': True}]}
    containt =list(like_comment_blog_info.keys())
    containt = containt[0]
    user_blog = db.user_blog.find()
    user_update = db.user_blog


    for data in user_blog:
        if containt in data:
            if str(like_comment_blog_info[containt][1]['comment']) is not None:
                comment_dict={like_comment_blog_info[containt][1]['comment']:like_comment_blog_info[containt][0]['mobile']}
                
                old_comment=[]
                old_comment =data[containt][0]['comment']
                old_comment.append(comment_dict)
                views = data['views']+1
                newvalues = { "$set": {containt:[{'comment':old_comment},{'likes':data[containt][1]['likes']},{'dislikes':data[containt][2]['dislikes']}],'views':views} }
                new_check = db.user_blog.find()
                for check in new_check:
                    if containt in check:
                        print(check,'======',newvalues)
                        x=user_update.update_one(check,newvalues)
                        print(x.modified_count)
            if like_comment_blog_info[containt][2]['likes']:
                print('inside likes')
                if like_comment_blog_info[containt][0]['mobile'] in data[containt][1]['likes']:
                    print("Already Liked")
                elif like_comment_blog_info[containt][0]['mobile']  in data[containt][2]['dislikes']:
                    print("you already disliked this")
                else:
                    user_info=like_comment_blog_info[containt][0]['mobile']
                    existing_likes_info = data[containt][1]['likes']
                    existing_likes_info.append(user_info)
                    views = data['views']+1
    #                 print(data['views'])
                    newvalues = { "$set": {containt:[{'comment':data[containt][0]['comment']},{'likes':existing_likes_info},{'dislikes':data[containt][2]['dislikes']}],'views':views} }

                    new_check = db.user_blog.find()
                    for check in new_check:
                            x=user_update.update_one(check,newvalues)
                            print(x.modified_count)            
            if like_comment_blog_info[containt][3]['dislikes']:
                print('inside dislikes')
                if like_comment_blog_info[containt][0]['mobile'] in data[containt][1]['likes']:
                    print("Already Liked")
                elif like_comment_blog_info[containt][0]['mobile']  in data[containt][2]['dislikes']:
                    print("you already disliked this")
                else:
                    user_info=like_comment_blog_info[containt][0]['mobile']
                    existing_likes_info = data[containt][2]['dislikes']
                    existing_likes_info.append(user_info)
                    views = data['views'] +1
    #                 print(data['views'])
                    newvalues = { "$set": {containt:[{'comment':data[containt][0]['comment']},{'likes':data[containt][1]['likes']},{'dislikes':existing_likes_info}],'views':views} }

                    new_check = db.user_blog.find()
                    for check in new_check:
                            x=user_update.update_one(check,newvalues)
                            print(x.modified_count)


    response ={"Status" : "Success" , "Message":"Blog has Been updated With requested Input","statusCode": 200}
        
    return JsonResponse(response)
                        
                
                    
               
        
#Logic For see all his uploaded blogs with views, likes, comments counts 

@csrf_exempt 
def all_user_uploded_blog_info(request):
        
    db = models.db_connection 
    db_collection = db.user_blog

    if "mobile" not in request.POST:
        response ={"Status" : False , "Message":"Please Enter Mobile Number  !!!","Status Code": 400}
        return JsonResponse(response)

    mobile = request.POST.get("mobile",None)

    user_blog = db.user_blog.find({"mobile":mobile})

    person_uploaded_blog_details = []
    for data in user_blog:
        person_uploaded_blog_details.append({"Uploaded_Blog":list(data.keys())[1],"views_Of_Blog":data['views'],"Likes":len(data[list(data.keys())[1]][1]['likes']),"Comments":data[list(data.keys())[1]][0]['comment']})
    
    response ={"Status" : "Success" , "person_uploaded_blog_details":person_uploaded_blog_details,"statusCode": 200}
        
    return JsonResponse(response)
    
    

        
#Logic For see the most viewed/ most liked / most commented blogs as a dashboard

@csrf_exempt 
def dashboard(request):

    db = models.db_connection 
    db_collection = db.user_blog   

    user_blog = db_collection.find()
    views =0
    likes =0
    commented =0
    most_views_blog =None
    most_likes_blog =None
    most_comments_blog =None

    for data in user_blog:
        if data['views']>views:
            views=data['views']
            most_views_blog =data
        if len(data[list(data.keys())[1]][1]['likes']) >likes:
            likes = len(data[list(data.keys())[1]][1]['likes'])
            most_comments_blog =data
        if len(data[list(data.keys())[1]][0]['comment']) >commented:  
            commented=len(data[list(data.keys())[1]][0]['comment'])
            most_comments_blog =data

    response ={"Status" : "Success" , "most_views_blog":most_views_blog,"most_likes_blog": most_likes_blog,"most_comments_blog":most_comments_blog, "statusCode": 200}
        
    return JsonResponse(response)        



#       3. For a particular blog, he can see who all like, dislike and the comments

@csrf_exempt 
def purticular_blog(request):
        
    if "blog" not in request.POST:
        response ={"Status" : False , "Message":"Please Enter Blog  !!!","Status Code": 400}
        return JsonResponse(response)

    blog_contain = request.POST.get("blog",None)

    # "This is My Second Sample Blog"
    user_blog = db.user_blog.find()
    temp_list =list(user_blog)
    count =len(temp_list)
    user_blog = db.user_blog.find()
    temp=1
    check_condition=False

    for data in user_blog:
        if blog_contain in list(data.keys())[1]:
            check_condition =True
            
        if count == temp:
            print('comming',check_condition)
            
            if check_condition:
                response ={"Status" : True , "Blog_Data":data,"Status Code": 400}
                return JsonResponse(response)


                
        temp =temp+1


 
            
            
            
        



















    # return HttpResponse({"Status":"Success"})

    # if word =="":
    #     design({"Status":"Please type any character"})
    #     return render(request,"result.html")

    # #READING GIVEN FILE AND CONVERT INTO DATAFRAME AND MAKE IT ITERABLE
    # data = pd.read_csv("/home/dfuser/Desktop/django/fuzzy/search_engine/word_search.tsv",sep='\t')

    # data.rename(columns={'the':'word','23135851162':'frequency'},inplace=4)

    # data['word'][333333]='the'
    # data['frequency'][333333] ='23135851162'

    # freq=[]
    # val=[]

    # dictonary = {'frequency':None,'value':None}        
    # for frequency,value in zip(data['frequency'],data['word']):
    #     if word in str(value):
    #         freq.append(frequency)
    #         val.append(value)

    # dictonary.update({'frequency':freq,'value':val})               
    # freq_temp=[]
    # val_temp=[]
    
     
    # try:  
    #     #COUNTING 25 WORD IF WORD HAS THAT MUTCH SUGGATIONS
    #     if len(dictonary['value'])>25:
    #         for data in dictonary['value']:
    #             if data.startswith(word):
    #                 index = dictonary['value'].index(data)
    #                 freq_temp.append(dictonary['frequency'][index])
    #                 val_temp.append(dictonary['value'][index])
    #     else:
    #         #LOGIC FOR LESS THEN 25 WORD AND FILTERING OUT WITH FREQUENCY AND SHORT WORD
    #         tuples_list = []
    #         for freq, val in zip(dictonary['frequency'],dictonary['value']):
    #             tuples_list.append((freq,val))
    #         result=Sort(tuples_list)
    #         dictonary ={}
    #         final_list =[]
            
    #         for data in result:
    #             final_list.append(data[1])
    #         for i in range(len(final_list)):
    #             for j in range(i,len(final_list)):
    #                 if (len(final_list[j])<len(final_list[i])):
    #                     temp = final_list[j]
    #                     final_list[j] =final_list[i]
    #                     final_list[i] =temp 
    #         if len(final_list) == 0:
    #             final_list.append("search word is not available in file")             
    #         dictonary.update({"Search_Result":final_list})  

    #         design(dictonary)
    #         return render(request,"result.html")
    #     #LOGIC FOR MORE THEN 25 WORD AND FILTERING OUT WITH FREQUENCY AND SHORT WORD

    #     dictonary.update({'frequency':freq_temp,'value':val_temp})   
    #     test_dict={}
    #     for freq ,val in zip(dictonary['frequency'], dictonary['value']):
    #         test_dict.update({freq:val})
    #     sorted_d = sorted(test_dict.items(), key=operator.itemgetter(0),reverse=True)
    #     my_list=[]
    #     dictonary= {}
    #     print(sorted_d)
    #     for i in range(0,25):
    #         my_list.append(sorted_d[i])   
    #     tup_data =Sort(my_list)
    #     final_list=[]

    #     for data in tup_data:
    #         final_list.append(data[1])   
    #     for i in range(len(final_list)):
    #         for j in range(i,len(final_list)):
    #             if len(final_list[j])<len(final_list[i]) and (final_list[j].startswith(word)):
    #                 temp = final_list[j]
    #                 final_list[j] =final_list[i]
    #                 final_list[i] =temp    
    #     dictonary.update({"Search_Result":final_list})  
        
    #     design(dictonary)
    #     # return render(request,"result.html")
    # except:      
    #     return render(request,"result.html")
 